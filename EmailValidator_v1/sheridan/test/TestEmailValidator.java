package test;

import static org.junit.Assert.*;

import org.junit.Test;

import src.emailValidator;

/**@author vishwa patel
 * @SID 991516280
 * 
 * Test classes for email validation
 * 
 */

public class TestEmailValidator {

	@Test
	public void testEmailStartsWIthcharacterRaguler() {
		boolean result = emailValidator.isValidEmail("vishwa@gmail.com");
		assertTrue("Email is starting with characters and not numbers", result );
		
	}

	@Test
	public void testEmailStartsWIthcharacterException() {
		boolean result = emailValidator.isValidEmail("1vishwa@gmail.com");
		assertFalse("Email should not start with number", result );
		
	}
	
	@Test
	public void testEmailStartsWIthcharacterBoundryIn() {
		boolean result = emailValidator.isValidEmail("vis@gmail.com");
		assertTrue("Email is starting with characters and not numbers", result );
		
	}
	
	@Test
	public void testEmailStartsWIthcharacterBoundryOut() {
		boolean result = emailValidator.isValidEmail("vishwapatel@gmail.com");
		assertTrue("Email is starting with characters and not numbers", result );
		
	}
	
	@Test
	public void testAccountHasThreeCharactersBoundryIn() {
		boolean result = emailValidator.isValidEmail("vis@gmail.com");
		assertTrue("Account name has 3 characters", result );
		
	}
	
	@Test
	public void testAccountHasThreeCharactersBoundryOut() {
		boolean result = emailValidator.isValidEmail("vishwa123@gmail.com");
		assertTrue("Account name has 3 characters", result );
	}
	
	@Test
	public void testEmailHasNoUpperCaseCharRaguler() {
		boolean result = emailValidator.isValidEmail("vishwa@gmail.com");
		assertTrue("Email is in lower case", result );
	}
	
	@Test
	public void testEmailHasNoUpperCaseCharException() {
		boolean result = emailValidator.isValidEmail("viShwa@gmail.com");
		assertFalse("Email contains one or many Upper case", result );
	}
	
	@Test
	public void testEmailHasNoUpperCaseCharBoundryIn() {
		boolean result = emailValidator.isValidEmail("vIshwa@gmail.com");
		assertFalse("Email is not in lower case", result );
	}
	
	@Test
	public void testEmailHasNoUpperCaseCharBoundryOut() {
		boolean result = emailValidator.isValidEmail("vISHwaPatel123@gmail.com");
		assertFalse("Email is not in lower case", result );
	}
	
	@Test
	public void testEmailHasOneAtCharRaguler() {
		boolean result = emailValidator.isValidEmail("vishwa@gmail.com");
		assertTrue("Email must only have one @ character", result );
	}
	
	@Test
	public void testEmailHasOneAtCharException() {
		boolean result = emailValidator.isValidEmail("vishwa@@gma@il.com");
		assertFalse("Email must only have one @ character", result );
	}
	
	@Test
	public void testEmailHasOneAtCharExceptionBoundryOut() {
		boolean result = emailValidator.isValidEmail("vishwa@@gmail.com");
		assertFalse("Email must only have one @ character", result );
	}
	
	@Test
	public void testEmailHasOneAtCharBoundryIn() {
		boolean result = emailValidator.isValidEmail("vishwagmail.com");
		assertFalse("Email must only have one @ character", result );
	}
	
	@Test
	public void testDomainHas3charactersRaguler() {
		boolean result = emailValidator.isValidEmail("vishwa@gmail.com");
		assertTrue("Domain name has 3 characters", result );
	}
	
	@Test
	public void testDomainHas3charactersExceptional() {
		boolean result = emailValidator.isValidEmail("vishwa@.com");
		assertFalse("Domain name does not have 3 characters", result );
	}
	
	@Test
	public void testDomainHas3charactersBoundryIn() {
		boolean result = emailValidator.isValidEmail("vishwa@gm.com");
		assertFalse("Domain name does not have 3 characters", result );
	}
	
	@Test
	public void testDomainHas3charactersBoundryOut() {
		boolean result = emailValidator.isValidEmail("vishwa@gmail123.com");
		assertTrue("Domain name has 3 characters", result );
	}
	
	@Test
	public void testExtensionHas2CharRaguler() {
		boolean result = emailValidator.isValidEmail("vishwa@gmail.com");
		assertTrue("Extansion has 2 characters", result );
	}

	
	@Test
	public void testExtensionHas2CharException() {
		boolean result = emailValidator.isValidEmail("vishwa@gmail.");
		assertFalse("Extansion does not have 2 characters", result );
	}
	
	@Test
	public void testExtensionHas2CharBoundryIn() {
		boolean result = emailValidator.isValidEmail("vishwa@gmail.c");
		assertFalse("Extansion does not have 2 characters", result );
	}
	
	@Test
	public void testExtensionHas2CharBoundryOut() {
		boolean result = emailValidator.isValidEmail("vishwa@gmail.commm");
		assertTrue("Extansion has 2 characters", result );
	}

	
}

