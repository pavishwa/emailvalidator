package src;

/**
 * 
 * @author vishwa patel
 * @SID 991516280
 * 
 * Email validator class.
 *
 */


public class emailValidator {
	
	public static boolean isValidEmail(String email) {
		  
		String regex = "^[a-z][0-9a-z][0-9a-z]+@[0-9a-z]{3,}(.+[a-z]{2,})$";
	     
	    return email.matches(regex);
	}
	
	 public static void main(String[] args) {
		 
	 }
	

}
